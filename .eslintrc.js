module.exports = {
  root: true,
  extends: [
    'airbnb', 
    'prettier',
    'plugin:react/recommended',
    'prettier/react',
    'plugin:prettier/recommended',
    'eslint-config-prettier'
  ],
  env: {
    'browser': true,
    'es2020': true,
    'es6': true,
    'react-native-react-native': true
  },
  plugins: [
    'prettier',
    'react',
    'react-native'
  ],
  parserOptions: {
    'ecmaFeatures': {
      'jsx': true
    },
    'ecmaVersion': 11,
    'sourceType': 'module'
  },
  rules: {
    'import/no-unresolved': 'off',
    'react/jsx-filename-extension': [
      1, 
      {
        'extensions': ['.js', '.jsx']
      }
    ],
    'prettier/prettier':[
      'error', 
      {
        'trailingComma': 'all',
        'bracketSpacing': true,
        'jsxBracketSameLine': false,
        'singleQuote': true,
        
      }
    ]
  }
};
