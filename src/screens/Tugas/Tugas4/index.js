import React, { 
    useState, 
    createContext 
} from 'react'
import TodoList from './TodoList'

export const RootContext = createContext()

const Context = () => {
    const [input, setInput] = useState('')
    const [todos, setTodos] = useState([])
    const [count, setCount] = useState(0)

    const handleChangeInput = (value) => setInput(value)

    const addTodo = () => {
        const date = new Date().getDate()
        const month = new Date().getMonth()
        const year = new Date().getFullYear()

        const today = `${date}/${month+1}/${year}`
        setTodos([
            ...todos,
            {
                id: count,
                title: input,
                date: today
            }
        ])
        setInput('')
        setCount(count + 1)
    }

    const deleteTodo = (id) => {
        setTodos((todo) => todo.filter((item) => item.id !== id))
    }

    return(
        <RootContext.Provider
            value={{
                todos,
                input,
                handleChangeInput,
                addTodo,
                deleteTodo,
            }}
        >
            <TodoList />

        </RootContext.Provider>
    )

}

export default Context
