import React, { useState, useEffect } from 'react'
import { NavigationContainer } from '@react-navigation/native'
import { createStackNavigator } from '@react-navigation/stack'
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs'
import Ionicons from 'react-native-vector-icons/Ionicons'
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons'
import SplashScreen from './Splashscreen'
import Intro from './Intro'
import Login from './Login'
import Home from '../Tugas6/Home'
import Profile from '../Tugas2/Biodata'
import Register from '../Tugas10/Register'
import Maps from '../Tugas12/Map'
import Chart from '../Tugas13/Chart'
import Chat from '../Tugas14/Chat'
import AsyncStorage from '@react-native-community/async-storage'
import auth from '@react-native-firebase/auth'

const Stack = createStackNavigator()
const Tabs = createBottomTabNavigator()

const HomeNavigation = () => (
    <Stack.Navigator>
        <Stack.Screen name="Home" component={TabNavigation} options={{ headerShown: false }} />
        <Stack.Screen name="Chart" component={Chart} options={({ route }) => ({ title: route.params.titleHeader })} />
    </Stack.Navigator>
)

const TabNavigation = () => (
    <Tabs.Navigator
        screenOptions={({ route }) => ({
            tabBarIcon: ({ focused, color, size }) => {
                let iconName

                if (route.name === 'Home') {
                    iconName = focused ? 'home' : 'home-outline'
                } else if (route.name === 'Maps') {
                    iconName = focused ? 'map-marker-radius' : 'map-marker-radius-outline'
                } else if (route.name === 'Chat') {
                    iconName = focused ? 'chat' : 'chat-outline'
                } else if (route.name === 'Profile') {
                    iconName = focused ? 'account' : 'account-outline'
                }
                return <MaterialCommunityIcons name={iconName} size={size} color={color} />
            }
        })}
        tabBarOptions={{
            style: {
                backgroundColor: '#fff',
            },
            labelStyle: {
                fontSize: 12,
                marginBottom: 5,
                fontWeight: 'bold',
            },
        }}
    >
        <Tabs.Screen name="Home" component={Home} />
        <Tabs.Screen name="Maps" component={Maps} />
        <Tabs.Screen name="Chat" component={Chat} />
        <Tabs.Screen name="Profile" component={Profile} />
    </Tabs.Navigator>
)


const AppNavigation = () => {
    const [firstLaunch, setFirstLaunch] = useState(null)
    const [initializing, setInitializing] = useState(true)
    const [user, setUser] = useState()

    const onAuthStateChanged = (user) => {
        setUser(user)
        if (initializing) setInitializing(false)
    }

    //mengatur durasi splashscreen saat aplikasi pertama kali dibuka
    useEffect(() => {
        AsyncStorage.getItem('alreadyLaunched').then((value) => {
            if (value == null) {
                AsyncStorage.setItem('alreadyLaunched', 'true')
                setFirstLaunch(true)
            } else {
                setFirstLaunch(false)
            }
        })

        const subscriber = auth().onAuthStateChanged(onAuthStateChanged)
        return subscriber
    }, [])

    if (initializing || firstLaunch === null) return <SplashScreen />

    const MainNavigation = () =>
        firstLaunch ? (
            <Stack.Navigator>
                <Stack.Screen name="Intro" component={Intro} options={{ headerShown: false }} />
                <Stack.Screen name="Login" component={Login} options={{ headerShown: false }} />
                <Stack.Screen name="Register" component={Register} options={{ headerShown: false }} />
            </Stack.Navigator>
        ) : (
                <Stack.Navigator>
                    <Stack.Screen name="Login" component={Login} options={{ headerShown: false }} />
                    <Stack.Screen name="Register" component={Register} options={{ headerShown: false }} />
                </Stack.Navigator>
            )

    return (
        <NavigationContainer>
            {!user ? <MainNavigation /> : <HomeNavigation />}
        </NavigationContainer>
    )
}

export default AppNavigation