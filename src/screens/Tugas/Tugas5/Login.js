import React, { useState, useEffect } from 'react'
import {
    View,
    Text,
    Image,
    TextInput,
    StatusBar,
    TouchableOpacity,
    ActivityIndicator
} from 'react-native'
import Ionicons from 'react-native-vector-icons/Ionicons'
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons'
import Axios from 'axios'
import AsyncStorage from '@react-native-community/async-storage'
import auth from '@react-native-firebase/auth'
import { GoogleSignin, statusCodes, GoogleSigninButton } from '@react-native-community/google-signin'
import TouchID from 'react-native-touch-id'
import { CommonActions } from '@react-navigation/native'
import { stylesLogin as styles } from '../../../style/styles'
import api from '../../../api'

const config = {
    title: 'Authentication Required',
    imageColor: '#191970',
    imageErrorColor: 'red',
    sensorDescription: 'Touch Sensor',
    sensorErrorDescription: 'Failed',
    cancelText: 'Cancel',
}

const Login = ({ navigation }) => {
    const [email, setEmail] = useState('')
    const [password, setPassword] = useState('')
    const [isLoading, setIsLoading] = useState(false)

    const saveToken = async (token) => {
        try {
            await AsyncStorage.setItem('token', token)
        } catch (e) {
            console.log(e)
        }
    }

    useEffect(() => {
        navigation.dispatch((state) => {
            const routes = state.routes.filter((route) => route.name !== 'Intro')

            return CommonActions.reset({
                ...state,
                routes,
                index: routes.length - 1,
            })
        })
        configureGoogleSignIn()
    }, [])

    const configureGoogleSignIn = () => {
        GoogleSignin.configure({
            offlineAccess: false,
            webClientId: '416425879758-ph57clcbj5qqppf4b14bfvj18t1fio9r.apps.googleusercontent.com',
        })
    }

    const signInWithGoogle = async () => {
        setIsLoading(true)

        try {
            const { idToken } = await GoogleSignin.signIn()
            setIsLoading(false)

            console.log('SignInWithGoogle -> idToken', idToken)

            const credential = auth.GoogleAuthProvider.credential(idToken)

            auth().signInWithCredential(credential)

            navigation.reset({
                index: 0,
                routes: [{ name: 'Home' }]
            })
        } catch (error) {
            setIsLoading(false)
            console.log('SignInWithGoogle -> error', error)
        }
    }

    const onLoginPress = () => {
        return auth().signInWithEmailAndPassword(email, password)
            .then((res) => {
                saveToken(res.user.uid)
                navigation.reset({
                    index: 0,
                    routes: [{ name: 'Home' }]
                })
            })
            .catch((error) => {
                alert(error)
                console.log("onLoginPress -> error", error)
            })
    }

    // const onLoginPress = () => {
    //     setIsLoading(true)
    //     let data = {
    //         email: email,
    //         password: password,
    //     }
    //     Axios.post(`${api}/login`, data, {timeout: 20000})
    //         .then((res) => {
    //             setIsLoading(false)
    //             saveToken(res.data.token)
    //             navigation.navigate('Home')
    //         })
    //         .catch((error) => {
    //             setIsLoading(false)
    //             alert("Login Failed")
    //             console.log('Login -> error', error)
    //         })
    // }


    const signInWithFingerprint = () => {
        TouchID.authenticate('', config)
            .then(success => {
                alert('Authentication Success')
                navigation.navigate('Home')
            })
            .catch(error => {
                alert('Authentication Error')
            })
    }

    if (isLoading) {
        return <ActivityIndicator size="large" color="#3EC6FF" style={styles.loading} />
    }

    return (
        <View style={styles.container}>
            <StatusBar backgroundColor="#ffffff" barStyle="dark-content" />
            <View style={styles.logoContainer}>
                <Image source={require('../../../assets/images/logo.jpg')} />
            </View>
            <View style={styles.formContainer}>
                <View style={styles.inputContainer}>
                    <Text>Username</Text>
                    <TextInput
                        value={email}
                        onChangeText={(email) => setEmail(email)}
                        style={styles.input}
                        placeholder="Username/ Password"
                    />
                </View>
                <View style={styles.inputContainer}>
                    <Text>Password</Text>
                    <TextInput
                        secureTextEntry={true}
                        value={password}
                        onChangeText={(password) => setPassword(password)}
                        style={styles.input}
                        placeholder="Password"
                    />
                </View>
                <View style={styles.inputContainer}>
                    <TouchableOpacity style={styles.buttonLogin} onPress={() => onLoginPress()}>
                        <MaterialCommunityIcons name='login' size={20} color='#fff'>
                            <Text style={styles.buttonText}>Login</Text>
                        </MaterialCommunityIcons>
                    </TouchableOpacity>
                </View>
                <View style={styles.formDecoration}>
                    <View style={styles.formDecoration2} />
                    <View>
                        <Text style={styles.formDecorationText}>OR</Text>
                    </View>
                    <View style={styles.formDecoration2} />
                </View>
                <View style={styles.inputContainer}>
                    <GoogleSigninButton
                        onPress={() => signInWithGoogle()}
                        style={{ width: '100%', height: 40 }}
                        size={GoogleSigninButton.Size.Wide}
                        color={GoogleSigninButton.Color.Dark}
                    />
                </View>
                <View style={styles.inputContainer}>
                    <TouchableOpacity style={styles.buttonLoginFingerprint} onPress={() => signInWithFingerprint()}>
                        <MaterialCommunityIcons name='fingerprint' size={20} color='#fff'>
                            <Text style={styles.buttonText}>Sign In With Fingerprint</Text>
                        </MaterialCommunityIcons>
                    </TouchableOpacity>
                </View>
                <View style={styles.footer}>
                    <Text>Belum mempunyai akun ? </Text>
                    <TouchableOpacity onPress={() => navigation.navigate('Register')}>
                        <Text style={{ color: '#3EC6FF' }}>Buat Akun</Text>
                    </TouchableOpacity>
                </View>
            </View>
        </View>
    )
}

export default Login