import React from 'react'
import { View, Image, StyleSheet, StatusBar } from "react-native"

const SplashScreen = () => {
    return (
        <View style={styles.container}>
            <StatusBar barStyle="dark-content" backgroundColor="#fff" />
            <View style={styles.logoContainer}>
                <Image source={require('../../../assets/images/logo.jpg')} />
            </View>
        </View>
    )
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: '#ffffff'
    },
    logoContainer: {
        padding: 20,
        alignItems: 'center',
        justifyContent: "center"
    }
})

export default SplashScreen