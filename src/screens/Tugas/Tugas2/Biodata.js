import React, { useState, useEffect } from 'react'
import {
    StyleSheet,
    View,
    Text,
    StatusBar,
    Dimensions,
    Image,
    TouchableOpacity,
} from 'react-native'
import {
    Colors,
} from 'react-native/Libraries/NewAppScreen'
import Icon from 'react-native-vector-icons/MaterialCommunityIcons'
import Axios from 'axios'
import AsyncStorage from '@react-native-community/async-storage'
import { GoogleSignin, statusCodes, GoogleSigninButton } from '@react-native-community/google-signin'
import auth from '@react-native-firebase/auth';
import api from '../../../api'

let deviceWidth = Dimensions.get('window').width
let deviceHeight = Dimensions.get('window').height

const Biodata = ({ navigation }) => {

    const [userInfo, setUserInfo] = useState(null)
    const [userToken, setUserToken] = useState(null)

    useEffect(() => {
        const getToken = async () => {
            try {
                const token = await AsyncStorage.getItem('token')
                setUserToken(token)
            } catch (error) {
                console.log(error)
            }
        }

        getToken()
        getCurrentUser()
    }, [userInfo])

    const getCurrentUser = async () => {
        const user = auth().currentUser
        if (user != null) setUserInfo(user)
        // try {
        //     const userInfo = await GoogleSignin.signInSilently()
        //     setUserInfo(userInfo)
        // } catch (error) {
        //     console.log(error)
        // }
    }

    const onLogoutPress = async () => {
        try {
            if (userToken != null) {
                await auth().signOut()
                await AsyncStorage.removeItem('token')
            } else {
                await GoogleSignin.revokeAccess()
                await GoogleSignin.signOut()
            }
            navigation.reset({
                index: 0,
                routes: [{ name: 'Login' }]
            })
        } catch (error) {
            console.log(error)
        }
    }

    return (
        <View style={styles.container}>
            <StatusBar barStyle="dark-content" backgroundColor="#fff" />
            <View style={styles.photoCard}>
                <Image source={{ uri: userInfo && userInfo.photoURL }} style={styles.profileImage} />
                <Text style={styles.profileText}>{userInfo && userInfo.displayName}</Text>
            </View>
            <View style={styles.content}>
                <View style={styles.profileContainer}>
                    <View style={styles.description}>
                        <View style={styles.descriptionTitle}><Text>Tanggal lahir</Text></View>
                        <View style={styles.descriptionText}><Text>05 December 1995</Text></View>
                    </View>
                    <View style={styles.description}>
                        <View style={styles.descriptionTitle}><Text>Jenis kelamin</Text></View>
                        <View style={styles.descriptionText}><Text>Laki - laki</Text></View>
                    </View>
                    <View style={styles.description}>
                        <View style={styles.descriptionTitle}><Text>Hobi</Text></View>
                        <View style={styles.descriptionText}><Text>Coding</Text></View>
                    </View>
                    <View style={styles.description}>
                        <View style={styles.descriptionTitle}><Text>No. Telp</Text></View>
                        <View style={styles.descriptionText}><Text>087854769001</Text></View>
                    </View>
                    <View style={styles.description}>
                        <View style={styles.descriptionTitle}><Text>Email</Text></View>
                        <View style={styles.descriptionText}><Text>{userInfo && userInfo.email}</Text></View>
                    </View>
                    <View style={StyleSheet.description}>
                        <TouchableOpacity style={styles.buttonLogout} onPress={() => onLogoutPress()}>
                            <Icon name='logout' size={25} color='#fff'>
                                <Text style={styles.buttonText}>Logout</Text>
                            </Icon>
                        </TouchableOpacity>
                    </View>
                </View>
            </View>
        </View>
    )
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        flexDirection: 'column',
        backgroundColor: Colors.lighter,
    },
    photoCard: {
        height: deviceHeight * 0.3,
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: '#3EC6FF'
    },
    content: {
        height: deviceHeight * 0.7
    },
    profileImage: {
        width: 100,
        height: 100,
        borderRadius: 100 / 2,
    },
    profileText: {
        color: '#fff',
        marginTop: 10,
        marginBottom: 10,
        fontSize: 20,
        fontWeight: 'bold',
    },
    profileContainer: {
        flex: 0.5,
        flexDirection: 'column',
        marginHorizontal: 30,
        marginTop: -40,
        elevation: 10,
        borderRadius: 10,
        shadowColor: '#000',
        shadowOffset: { width: 0, height: 1 },
        shadowOpacity: 0.8,
        shadowRadius: 2,
        height: deviceHeight * 0.2,
        backgroundColor: '#fff'
    },
    description: {
        flex: 0.3,
        flexDirection: 'row',
    },
    descriptionTitle: {
        flex: 0.3,
        marginTop: 10,
        marginHorizontal: 10,
        alignItems: 'flex-start'
    },
    descriptionText: {
        flex: 0.7,
        marginTop: 10,
        marginHorizontal: 10,
        alignItems: 'flex-end',
    },
    buttonLogout: {
        backgroundColor: '#3EC6FF',
        marginHorizontal: 10,
        marginVertical: 10,
        alignItems: 'center',
        justifyContent: 'center',
        padding: 5,
        borderRadius: 5,
    },
    buttonText: {
        fontSize: 18,
        fontWeight: 'bold',
        color: '#fff',
        textTransform: 'uppercase'
    },
})

export default Biodata;