import React from 'react'
import { View, Text, StyleSheet, StatusBar, Image, ScrollView, TouchableOpacity } from 'react-native'
import Ionicons from 'react-native-vector-icons/Ionicons'
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons'
import { stylesHome as styles } from '../../../style/styles'

const HomeScreen = ({ navigation }) => {

    const RenderClassFirst = () => {
        return (
            <View style={styles.classCard}>
                <View style={styles.cardTitle}>
                    <Text style={styles.titleText}>Kelas</Text>
                </View>
                <View style={styles.cardContent}>
                    <View style={styles.classContent}>
                        <TouchableOpacity onPress={() => navigation.navigate('Chart', { titleHeader: 'React Native' })}>
                            <Ionicons name="logo-react" size={45} color={'#fff'} />
                            <Text style={styles.classDescription}>React Native</Text>
                        </TouchableOpacity>
                    </View>
                    <View style={styles.classContent}>
                        <Ionicons name="logo-python" size={45} color={'white'} />
                        <Text style={styles.classDescription}>Data Science</Text>
                    </View>
                    <View style={styles.classContent}>
                        <Ionicons name="logo-react" size={45} color={'white'} />
                        <Text style={styles.classDescription}>React JS</Text>
                    </View>
                    <View style={styles.classContent}>
                        <Ionicons name="logo-laravel" size={45} color={'white'} />
                        <Text style={styles.classDescription}>Laravel</Text>
                    </View>
                </View>
            </View>
        )
    }

    const RenderClassSecond = () => {
        return (
            <View style={styles.classCard}>
                <View style={styles.cardTitle}>
                    <Text style={styles.titleText}>Kelas</Text>
                </View>
                <View style={styles.cardContent}>
                    <View style={styles.classContent}>
                        <Ionicons name="logo-wordpress" size={45} color={'#fff'} />
                        <Text style={styles.classDescription}>Wordpress</Text>
                    </View>
                    <View style={styles.classContent}>
                        <Image
                            style={styles.imageIcon}
                            source={require('../../../assets/images/website-design.png')}
                        />
                        <Text style={styles.classDescription}>Design Grafis</Text>
                    </View>
                    <View style={styles.classContent}>
                        <MaterialCommunityIcons name="server" size={45} color={'white'} />
                        <Text style={styles.classDescription}>Web Server</Text>
                    </View>
                    <View style={styles.classContent}>
                        <Image
                            style={styles.imageIcon}
                            source={require('../../../assets/images/ux.png')}
                        />
                        <Text style={styles.classDescription}>UI/UX Design</Text>
                    </View>
                </View>
            </View>
        )
    }

    const RenderSummary = () => {
        return (
            <View style={styles.summaryCard}>
                <View style={styles.cardTitle}>
                    <Text style={styles.titleText}>Summary</Text>
                </View>
                <ScrollView style={styles.scrollView} showsVerticalScrollIndicator={false}>
                    <View style={styles.summaryTitle}>
                        <Text style={styles.titleText}>React Native</Text>
                    </View>
                    <View style={styles.summaryContent}>
                        <View style={styles.summaryRowContent}>
                            <Text style={styles.summaryTextContent}>Today</Text>
                            <Text style={styles.summaryTextContent}>20 orang</Text>
                        </View>
                        <View style={styles.summaryRowContent}>
                            <Text style={styles.summaryTextContent}>Total</Text>
                            <Text style={styles.summaryTextContent}>100 orang</Text>
                        </View>
                    </View>
                    <View style={styles.summaryTitle}>
                        <Text style={styles.titleText}>Data Science</Text>
                    </View>
                    <View style={styles.summaryContent}>
                        <View style={styles.summaryRowContent}>
                            <Text style={styles.summaryTextContent}>Today</Text>
                            <Text style={styles.summaryTextContent}>30 orang</Text>
                        </View>
                        <View style={styles.summaryRowContent}>
                            <Text style={styles.summaryTextContent}>Total</Text>
                            <Text style={styles.summaryTextContent}>100 orang</Text>
                        </View>
                    </View>
                    <View style={styles.summaryTitle}>
                        <Text style={styles.titleText}>ReactJs</Text>
                    </View>
                    <View style={styles.summaryContent}>
                        <View style={styles.summaryRowContent}>
                            <Text style={styles.summaryTextContent}>Today</Text>
                            <Text style={styles.summaryTextContent}>66 orang</Text>
                        </View>
                        <View style={styles.summaryRowContent}>
                            <Text style={styles.summaryTextContent}>Total</Text>
                            <Text style={styles.summaryTextContent}>100 orang</Text>
                        </View>
                    </View>
                    <View style={styles.summaryTitle}>
                        <Text style={styles.titleText}>Laravel</Text>
                    </View>
                    <View style={styles.summaryContent}>
                        <View style={styles.summaryRowContent}>
                            <Text style={styles.summaryTextContent}>Today</Text>
                            <Text style={styles.summaryTextContent}>60 orang</Text>
                        </View>
                        <View style={styles.summaryRowContent}>
                            <Text style={styles.summaryTextContent}>Total</Text>
                            <Text style={styles.summaryTextContent}>100 orang</Text>
                        </View>
                    </View>
                    <View style={styles.summaryTitle}>
                        <Text style={styles.titleText}>Wordpress</Text>
                    </View>
                    <View style={styles.summaryContent}>
                        <View style={styles.summaryRowContent}>
                            <Text style={styles.summaryTextContent}>Today</Text>
                            <Text style={styles.summaryTextContent}>60 orang</Text>
                        </View>
                        <View style={styles.summaryRowContent}>
                            <Text style={styles.summaryTextContent}>Total</Text>
                            <Text style={styles.summaryTextContent}>100 orang</Text>
                        </View>
                    </View>
                    <View style={styles.summaryTitle}>
                        <Text style={styles.titleText}>Design Grafis</Text>
                    </View>
                    <View style={styles.summaryContent}>
                        <View style={styles.summaryRowContent}>
                            <Text style={styles.summaryTextContent}>Today</Text>
                            <Text style={styles.summaryTextContent}>60 orang</Text>
                        </View>
                        <View style={styles.summaryRowContent}>
                            <Text style={styles.summaryTextContent}>Total</Text>
                            <Text style={styles.summaryTextContent}>100 orang</Text>
                        </View>
                    </View>
                    <View style={styles.summaryTitle}>
                        <Text style={styles.titleText}>Web Server</Text>
                    </View>
                    <View style={styles.summaryContent}>
                        <View style={styles.summaryRowContent}>
                            <Text style={styles.summaryTextContent}>Today</Text>
                            <Text style={styles.summaryTextContent}>60 orang</Text>
                        </View>
                        <View style={styles.summaryRowContent}>
                            <Text style={styles.summaryTextContent}>Total</Text>
                            <Text style={styles.summaryTextContent}>100 orang</Text>
                        </View>
                    </View>
                    <View style={styles.summaryTitle}>
                        <Text style={styles.titleText}>UI/UX Design</Text>
                    </View>
                    <View style={styles.summaryContent}>
                        <View style={styles.summaryRowContent}>
                            <Text style={styles.summaryTextContent}>Today</Text>
                            <Text style={styles.summaryTextContent}>60 orang</Text>
                        </View>
                        <View style={styles.summaryRowContent}>
                            <Text style={styles.summaryTextContent}>Total</Text>
                            <Text style={styles.summaryTextContent}>100 orang</Text>
                        </View>
                    </View>
                </ScrollView>
            </View>
        )
    }

    return (
        <View style={styles.container}>
            <StatusBar barStyle="dark-content" backgroundColor="#fff" />
            <RenderClassFirst />
            <RenderClassSecond />
            <RenderSummary />
        </View>
    )
}

export default HomeScreen