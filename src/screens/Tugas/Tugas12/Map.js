import React, {useEffect} from 'react'
import { View, Text } from 'react-native'
import MapboxGL from '@react-native-mapbox-gl/maps'
import {stylesMaps as styles} from '../../../style/styles'

MapboxGL.setAccessToken(`pk.eyJ1IjoiYXJub2xkYXJtYW5kbzA3IiwiYSI6ImNqbWV5ZW5ueTFvMjAzcWxodWk5aW1pNWkifQ.vpy9jJBkKHo_CJpC9XXcYw`)

const Map = () => {

    useEffect(() => {
        const getLocation = async () => {
            try {
                const permission = await MapboxGL.requestAndroidLocationPermissions()
            } catch (error) {
                console.log(error)
            }
        }
        getLocation()
    }, [])


    const coordinates = [
        [107.598827, -6.896191],
        [107.596198, -6.899688],
        [107.618767, -6.902226],
        [107.621095, -6.898690],
        [107.615698, -6.896741],
        [107.613544, -6.897713],
        [107.613697, -6.893795],
        [107.610714, -6.891356],
        [107.605468, -6.893124],
        [107.609180, -6.898013]
    ]

    const renderPointAnnotation = (index) => {
        const id = `point_${index}`
        const coordinate = coordinates[index]
        const title = `Longitude : ${coordinates[index][0]}\nLatitue : ${coordinates[index][1]}`
        return (
            <MapboxGL.PointAnnotation 
                key={id}
                id={id}
                coordinate={coordinate}    
            >
                <MapboxGL.Callout title={title} />
            </MapboxGL.PointAnnotation>
        )
    }

    const renderAnnotation = () => {
        const points = []

        for (let i=0; i < coordinates.length; i++){
            points.push(renderPointAnnotation(i))
        }

        return points
    }

    return(
        <View style={styles.container}>
            <MapboxGL.MapView style={{ flex: 1 }}>
                <MapboxGL.UserLocation 
                    visible={true}
                />
                <MapboxGL.Camera 
                    followUserLocation={true}
                />
                {renderAnnotation()}
            </MapboxGL.MapView>
        </View>
    )
}

export default Map