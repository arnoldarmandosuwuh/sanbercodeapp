import React from 'react'
import {
  StyleSheet,
  View,
  Text,
  StatusBar,
} from 'react-native'
import {
  Colors,
} from 'react-native/Libraries/NewAppScreen'

const Intro = () => {
    return(
        <View style={styles.container}>
          <StatusBar barStyle="dark-content" />
          <Text>Hallo Kelas React Native Lanjutan Sanbercode!</Text>
        </View>
    )
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: Colors.lighter,
  }
})
  
export default Intro