import React, { useEffect, useState } from 'react'
import { View, Text, StyleSheet } from 'react-native'
import { GiftedChat } from 'react-native-gifted-chat'
import auth from '@react-native-firebase/auth'
import database from '@react-native-firebase/database'

const Chat = ({route, navigation}) => {
    
    const [messages, setMessages] = useState([])
    const [user, setUser] = useState({})

    useEffect(() => {
        const user = auth().currentUser
        setUser(user)
        getData()
        return () => {
            const db = database().ref('messages')
            if(db){
                db.off()
            }
        }
    }, [])

    const onSend = ((messages = []) => {
        for(let i=0; i< messages.length; i++){
            database().ref('messages').push({
                _id: messages[i]._id,
                createdAt: database.ServerValue.TIMESTAMP,
                text: messages[i].text,
                user: messages[i].user
            })
        }
    })

    const getData = () => {
        database().ref('messages').limitToLast(20).on('child_added', snapshot => {
            const value = snapshot.val()
            setMessages(previousMessages => GiftedChat.append(previousMessages, value))
        })
    }
    
    return(
        <GiftedChat
            messages={messages}
            onSend={messages => onSend(messages)}
            user={{
                _id: user.uid,
                name: user.email,
                avatar: 'https://instagram.fsub8-1.fna.fbcdn.net/v/t51.2885-19/s150x150/39300563_447500175655867_7444333678760558592_n.jpg?_nc_ht=instagram.fsub8-1.fna.fbcdn.net&_nc_cat=107&_nc_ohc=rFnvopTdCycAX8KLBZ_&oh=29ca0753d0e3ba3ffb8da2072ea398fd&oe=5F530753'            
            }}
        />
    )
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#fff'
    }
})

export default Chat