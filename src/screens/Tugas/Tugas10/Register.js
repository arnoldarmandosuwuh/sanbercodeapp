import React, { useState } from 'react'
import {
  View,
  Text,
  Image,
  Modal,
  StatusBar,
  TextInput,
  TouchableOpacity,
} from 'react-native'
import { RNCamera } from 'react-native-camera'
import storage from '@react-native-firebase/storage'
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons'
import Feather from 'react-native-vector-icons/Feather'
import { stylesRegister as styles } from '../../../style/styles'

const Register = ({ navigation }) => {
  const [isVisible, setIsVisible] = useState(false)
  const [type, setType] = useState('back')
  const [photo, setPhoto] = useState(null)

  const toogleCamera = () => {
    setType(type === 'back' ? 'front' : 'back')
  }

  const takePicture = async () => {
    const options = { quality: 0.5, base64: true }
    if (camera) {
      const data = await camera.takePictureAsync(options)
      console.log('takePicture -> data', data)
      setPhoto(data)
      setIsVisible(false)
    }
  }

  const uploadImage = (uri) => {
    const sessionId = new Date().getTime()
    return storage()
      .ref(`images/${sessionId}`)
      .putFile(uri)
      .then((response) => {
        alert(`Upload Success`);
        navigation.navigate('Login')
      })
      .catch((error) => {
        alert(error)
      })
  }

  const RenderCamera = () => {
    return (
      <Modal visible={isVisible} onRequestClose={() => setIsVisible(false)}>
        <View style={{ flex: 1 }}>
          <RNCamera
            style={{ flex: 1 }}
            ref={(ref) => {
              camera = ref
            }}
            type={type}>
            <View style={styles.btnFlipContainer}>
              <TouchableOpacity
                style={styles.btnFlip}
                onPress={() => toogleCamera()}>
                <MaterialCommunityIcons name="rotate-3d-variant" size={15} />
              </TouchableOpacity>
            </View>
            <View style={styles.round} />
            <View style={styles.rectangle} />
            <View style={styles.btnTakeContainer}>
              <TouchableOpacity
                style={styles.btnTake}
                onPress={() => takePicture()}>
                <Feather name="camera" size={30} />
              </TouchableOpacity>
            </View>
          </RNCamera>
        </View>
      </Modal>
    )
  }
  return (
    <View style={styles.container}>
      <StatusBar backgroundColor="#3EC6FF" barStyle="dark-content" />
      <View style={styles.blueContainer}>
        <Image
          source={
            photo === null
              ? require('../../../assets/images/default_photo.jpg')
              : { uri: photo.uri }
          }
          style={styles.imgProfile}
        />
        <TouchableOpacity
          style={styles.changePicture}
          onPress={() => {
            setIsVisible(true);
          }}>
          <Text style={styles.btnChangePicture}>Change Picture</Text>
        </TouchableOpacity>
      </View>
      <View style={styles.cardContainer}>
        <View style={styles.inputContainer}>
          <Text style={styles.titleForm}>Nama</Text>
          <TextInput style={styles.textInput} placeholder="Name" />
        </View>
        <View style={styles.inputContainer}>
          <Text style={styles.titleForm}>Email</Text>
          <TextInput style={styles.textInput} placeholder="Email" />
        </View>
        <View style={styles.inputContainer}>
          <Text style={styles.titleForm}>Password</Text>
          <TextInput
            secureTextEntry={true}
            style={styles.textInput}
            placeholder="Password"
          />
        </View>
        <TouchableOpacity
          style={styles.buttonRegister}
          onPress={() => uploadImage(photo.uri)}>
          <MaterialCommunityIcons name="logout" size={25} color="#fff">
            <Text style={styles.buttonText}>Register</Text>
          </MaterialCommunityIcons>
        </TouchableOpacity>
      </View>
      <RenderCamera />
    </View>
  )
}

export default Register
