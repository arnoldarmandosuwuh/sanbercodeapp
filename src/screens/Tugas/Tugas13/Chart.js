import React, {useState} from 'react'
import {View, Text, processColor, StyleSheet} from 'react-native'
import {BarChart} from "react-native-charts-wrapper"

const Chart = ({ navigation }) => {
    
    const marker = (data) => {
        return{
            y: data,
            marker: [`React Native Dasar \n${data[0]}`, `React Native Lanjutan \n${data[1]}`]
        }
    }
    
    const data = [
        marker([100, 40]),
        marker([80, 60]),
        marker([40, 90]),
        marker([78, 45]),
        marker([67, 87]),
        marker([98, 32]),
        marker([150, 90]),
    ]

    const [legend, setLegend] = useState({
        enabled: true,
        textSize: 14,
        form: 'SQUARE',
        formSize: 14,
        xEntrySpace: 10,
        yEntrySpace: 5,
        formToTextSpace: 5,
        wordWrapEnabled: true,
        maxSizePercent: 0.5
    })

    const [chart, setChart] = useState({
        data: {
            dataSets: [{
                values: data,
                label: '',
                config: {
                    colors: [processColor('#3EC6FF'), processColor('#088dc4')],
                    stackLabels: ['React Native Dasar', 'React Native Lanjutan'],
                    drawFilled: false,
                    drawValues: false,
                }
            }]
        }
    })

    const [xAxis, setXAxis] = useState({
        valueFormatter: ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Agu', 'Sep', 'Oct', 'Nov', 'Des'],
        position: 'BOTTOM',
        drawAxisLine: true,
        drawGridLines: false,
        axisMinimum: -0.5,
        granularityEnabled: true,
        granularity: 1,
        axisMaximum: new Date().getMonth() + 0.5,
        spaceBetweenLabels: 0,
        labelRotationAngle: -45.0,
        limitLines: [{ limit: 115, lineColor: processColor('red'), lineWidth: 1 }]
    })

    const [yAxis, setYAxis] = useState({
        left: {
            axisMinimum: 0,
            labelCountForce: true,
            granularity: 5,
            granularityEnabled: true,
            drawGridLines: false
        },
        right: {
            axisMinimum: 0,
            labelCountForce: true,
            granularity: 5,
            granularityEnabled: true,
            enabled: false
        }
    })
    return (
        <View style={styles.container}>
            <BarChart
                style={{flex:1}}
                data={chart.data}
                legend={legend}
                xAxis={xAxis}
                yAxis={yAxis}
                marker={{enabled:true}}
            >
            </BarChart>
        </View>
    )
}
const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#fff',
    }
})

export default Chart