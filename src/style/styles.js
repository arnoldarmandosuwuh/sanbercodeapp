import React from 'react'
import { StyleSheet, Dimensions } from "react-native"

const height = Dimensions.get("window").height;
const width = Dimensions.get("window").width;

export const stylesIntro = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        backgroundColor: '#fff'
    },
    slide: {
        flex: 1,
        backgroundColor: '#fff',
        alignItems: 'center',
        justifyContent: 'center',
        padding: 10,
    },
    title: {
        color: '#191970',
        fontWeight: 'bold',
        fontSize: 20,
    },
    image: {
        width: 300,
        height: 300,
        marginTop: 15,
    },
    text: {
        marginTop: 15,
        color: '#000',
        textAlign: 'center',
        fontSize: 16,
    },
    buttonCircle: {
        width: 50,
        height: 50,
        backgroundColor: '#191970',
        justifyContent: 'center',
        alignItems: 'center',
        borderRadius : 200 / 2
    }
})

export const stylesLogin = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#fff'
    },
    logoContainer: {
        padding: 20,
        alignItems: 'center',
        justifyContent: 'center'
    },
    formContainer: {
        flex: 1,
        padding: 20,
        width: '100%',
    },
    inputContainer: {
        marginBottom: 10,
    },
    input: {
        borderBottomColor: '#000', 
        borderBottomWidth: 1,
    },
    formDecoration: {
        flexDirection: 'row',
        alignItems: 'center',
        marginBottom: 10,
    },
    formDecoration2: {
        flex: 1, 
        height: 1, 
        backgroundColor: '#000'
    },
    formDecorationText: {
        width: 50, 
        textAlign: 'center' 
    },
    buttonLogin: {
        backgroundColor: '#3EC6FF',
        alignItems: 'center',
        justifyContent: 'center',
        padding: 5,
        borderRadius: 5,
    }, 
    buttonLoginFingerprint: {
        backgroundColor: '#191970',
        alignItems: 'center',
        justifyContent: 'center',
        padding: 5,
        borderRadius: 5,
    }, 
    buttonText: {
        color: '#fff',
        fontWeight: 'bold',
        fontSize: 18,
        textTransform: 'uppercase'
    },
    buttonLoginGoogle: {
        backgroundColor: '#fc0303',
        alignItems: 'center',
        justifyContent: 'center',
        padding: 5,
        borderRadius: 5,
    }, 
    footer: {
        flex: 1,
        flexDirection: 'row',
        marginHorizontal: 24,
        marginBottom: 24,
        alignItems: 'flex-end',
        justifyContent: 'center',
    },
})

export const stylesRegister = StyleSheet.create({
    btnFlipContainer:{
        margin: 20,
    },
    btnFlip: {
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: 'white',
        height: 50,
        width: 50,
        borderRadius: 50          
    },
    round: {
        width: '50%',
        height: '45%',
        borderRadius: 100,
        borderWidth: 1,
        borderColor: '#fff',
        alignSelf: 'center',
    },
    rectangle: {
        marginTop: 60,
        width: '50%',
        height: '20%',
        borderWidth: 1,
        borderColor: '#fff',
        alignSelf: 'center',
    },
    btnTakeContainer: {
        alignItems: 'center',
    },
    btnTake: {
        marginTop: 20,
        height: 80,
        width: 80,
        borderRadius: 50,
        backgroundColor: '#fff',
        justifyContent: 'center',
        alignItems: 'center'
    },
    container: {
        flex: 1,
        alignItems: 'center',
        backgroundColor: '#fff'
    },
    blueContainer: {
        width: width,
        height: height * 0.3,
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: '#3EC6FF'
    },
    imgProfile: {
        width: 100,
        height: 100,
        borderRadius: 100 / 2,
    },
    changePicture: {
        marginVertical: 20,
        alignItems: 'center',
        justifyContent: 'center',
    },
    btnChangePicture: {
        color: '#fff',
        fontSize: 18
    },
    cardContainer: {
        width: width * 0.8,
        marginHorizontal: 30,
        marginTop: -40,
        elevation: 10,
        borderRadius: 10,
        padding: 5,
        shadowColor: '#000',
        shadowOffset: { width: 0, height: 1 },
        shadowOpacity: 0.8,
        shadowRadius: 8,  
        backgroundColor: '#fff'
    },
    inputContainer:{
        marginVertical: 10,
        marginHorizontal: 20
    },
    titleForm: {
        fontWeight: 'bold',
    },
    textInput:{
        borderBottomWidth: 1,
        marginTop: 10,
        padding:-5
    },
    buttonRegister: {
        backgroundColor: '#3EC6FF',
        marginHorizontal: 20,
        marginVertical: 10,
        alignItems: 'center',
        justifyContent: 'center',
        padding: 5,
        borderRadius: 5,
    },
    buttonText: {
      fontSize: 18,
      fontWeight: 'bold',
      color: '#fff',
      textTransform: 'uppercase',
      textAlign: 'center'
    },
})

export const stylesHome = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: 'center',
        backgroundColor: '#ffffff'
    },
    classCard: {
        display: 'flex',
        width: width * 0.95,
        marginVertical: 5
    },
    cardTitle: {
        backgroundColor: "#088dc4",
        borderTopLeftRadius: 10,
        borderTopRightRadius: 10,
        height: 30,
        justifyContent: 'center'
    },
    titleText: {
        color: '#fff', 
        paddingLeft: 10
    },
    cardContent: {
        backgroundColor: '#3EC6FF',
        flexDirection: 'row',
        paddingHorizontal: 10,
        justifyContent: 'space-between',
        paddingVertical: 10,
        borderBottomLeftRadius: 10,
        borderBottomRightRadius: 10
    },
    classContent: {
        flexDirection: 'column',
        justifyContent: 'center',
        alignItems: 'center'
    },
    classDescription: {
        color: '#fff',
        fontSize: 10
    },
    imageIcon: {
        width: 45, 
        height: 45 
    },
    summaryCard: {
        display: 'flex',
        width: width * 0.95,
        marginVertical: 5,
        height: height * 0.63,
    },
    scrollView: {
        display: 'flex',
    },
    summaryTitle: {
        backgroundColor: '#3EC6FF',
        height: 30,
        justifyContent: 'center'
    },
    summaryContent: {
        display: 'flex',
        backgroundColor: "#088dc4",
        paddingVertical: 5
    },
    summaryTextContent: {
        color: 'white'
    },
    summaryRowContent: {
        display: 'flex',
        flexDirection: 'row',
        justifyContent: 'space-between',
        paddingHorizontal: 30,
        marginVertical: 2
    }
})

export const stylesMaps = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#ffffff'
    },
})