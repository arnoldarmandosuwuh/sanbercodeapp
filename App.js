/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */

import React, { useEffect } from "react";
import firebase from "@react-native-firebase/app";
import Intro from "./src/screens/Tugas/Tugas1/Intro";
import Biodata from "./src/screens/Tugas/Tugas2/Biodata";
import TodoList from "./src/screens/Tugas/Tugas3/TodoList";
import Context from "./src/screens/Tugas/Tugas4/index";
import Screens from "./src/screens/Tugas/Tugas5/navigation";
import OneSignal from "react-native-onesignal";
import codePush from "react-native-code-push";
import { Alert } from "react-native";

// code-push release-react SanbercodeApp android
// code-push deployment list SanbercodeApp
// code-push promote SanbercodeApp Staging Production

var firebaseConfig = {
  apiKey: "AIzaSyBLVP4dUrf52332vzY6xl4CI8mhm9Sy2Oc",
  authDomain: "sanbercode-89b39.firebaseapp.com",
  databaseURL: "https://sanbercode-89b39.firebaseio.com",
  projectId: "sanbercode-89b39",
  storageBucket: "sanbercode-89b39.appspot.com",
  messagingSenderId: "416425879758",
  appId: "1:416425879758:web:c26547ec4b0f8fbd09889f",
  measurementId: "G-3XKBTQZM7K",
};
// Initialize Firebase
if (!firebase.apps.length) {
  firebase.initializeApp(firebaseConfig);
}

const App = () => {
  useEffect(() => {
    OneSignal.setLogLevel(6, 0);

    OneSignal.init("d8fb3314-8499-4d85-b6d4-04232b78e3c5", {
      kOSSettingsKeyAutoPrompt: false,
      kOSSettingsKeyInAppLaunchURL: false,
      kOSSettingsKeyInFocusDisplayOption: 2,
    });
    OneSignal.inFocusDisplaying(2);

    OneSignal.addEventListener("received", onReceived);
    OneSignal.addEventListener("opened", onOpened);
    OneSignal.addEventListener("ids", onIds);

    codePush.sync({
      updateDialog: true,
      installMode: codePush.InstallMode.IMMEDIATE,
    }, SyncStatus);

    return () => {
      OneSignal.removeEventListener("received", onReceived);
      OneSignal.removeEventListener("opened", onOpened);
      OneSignal.removeEventListener("ids", onIds);
    };
  }, []);

  const SyncStatus = (status) => {
    switch (status) {
      case codePush.SyncStatus.CHECKING_FOR_UPDATE:
        console.log("Checking Update");
        break;

      case codePush.SyncStatus.DOWNLOADING_PACKAGE:
        console.log("Downloadin Package");
        break;

      case codePush.SyncStatus.UP_TO_DATE:
        console.log("Up to date");
        break;

      case codePush.SyncStatus.INSTALLING_UPDATE:
        console.log("Installing Update");
        break;

      case codePush.SyncStatus.UPDATE_INSTALLED:
        Alert.alert("Notification", "Update Installed");
        break;

      case codePush.SyncStatus.AWAITING_USER_ACTION:
        console.log("Awaiting user action");
        break;
      default:
        break;
    }
  };

  const onReceived = (notification) => {
    console.log("onReceived -> notification", notification);
  };

  const onOpened = (openResult) => {
    console.log("onOpened -> openResult", openResult);
  };

  const onIds = (device) => {
    console.log("onIds -> device", device);
  };

  return (
    <Screens />
  );
};

export default App;
